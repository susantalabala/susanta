//
//  ViewController.swift
//  swiftWithCPP
//
//  Created by intimetec on 25/01/16.
//  Copyright © 2016 intimetec. All rights reserved.
//

import UIKit


//import TestCpp.h
//import CPPClassWrapper.h

//let wrapperItem = TestCppClassWrapper(title: "Init test text for cpp item wrapper class")
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      let wrapperItem = TestCppClassWrapper(title: "Init test text for cpp item wrapper class")
        print("Title: \(wrapperItem.getTitle())")
        wrapperItem.setTitle("Just yet another test text setted after cpp item wrapper class init")
        print("Title: \(wrapperItem.getTitle())")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

 