//
//  CPPClassWrapper.m
//  swiftWithCPP
//
//  Created by intimetec on 25/01/16.
//  Copyright © 2016 intimetec. All rights reserved.
//


#import "CPPClassWrapper.h"
#include "TestCPP.h"
@interface TestCppClassWrapper()
@property TestCppClass *cppItem;
@end

@implementation TestCppClassWrapper
- (instancetype)initWithTitle:(NSString*)title
{
    if (self = [super init]) {
        self.cppItem = new TestCppClass(std::string([title cStringUsingEncoding:NSUTF8StringEncoding]));
    }
    return self;
}
- (NSString*)getTitle
{
    return [NSString stringWithUTF8String:self.cppItem->getTtile().c_str()];
}
- (void)setTitle:(NSString*)title
{
    self.cppItem->setTitle(std::string([title cStringUsingEncoding:NSUTF8StringEncoding]));
}
@end
