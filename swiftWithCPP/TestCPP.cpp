//
//  TestCPP.cpp
//  swiftWithCPP
//
//  Created by intimetec on 25/01/16.
//  Copyright © 2016 intimetec. All rights reserved.
//

#include "TestCPP.h"

TestCppClass::TestCppClass() {}
TestCppClass::TestCppClass(const std::string &title): m_title(title) {}
TestCppClass::~TestCppClass() {}
void TestCppClass::setTitle(const std::string &title)
{
    m_title = title;
}
const std::string &TestCppClass::getTtile()
{
    return m_title;
}