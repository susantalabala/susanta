//
//  CPPClassWrapper.h
//  swiftWithCPP
//
//  Created by intimetec on 25/01/16.
//  Copyright © 2016 intimetec. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface TestCppClassWrapper : NSObject
- (instancetype)initWithTitle:(NSString*)title;
- (NSString*)getTitle;
- (void)setTitle:(NSString*)title;
@end