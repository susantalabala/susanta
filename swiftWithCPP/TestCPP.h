//
//  TestCPP.hpp
//  swiftWithCPP
//
//  Created by intimetec on 25/01/16.
//  Copyright © 2016 intimetec. All rights reserved.
//

#ifndef TestCPP_h
#define TestCPP_h

#include <stdio.h>
#include <string>
class TestCppClass {
public:
    TestCppClass();
    TestCppClass(const std::string &title);
    ~TestCppClass();
    
public:
    void setTitle(const std::string &title);
    const std::string &getTtile();
    
private:
    std::string m_title;
};



#endif /* TestCPP_hpp */
